# About Simpl-Labs

## Welcome

This is a group where we develop a [Simpl project](https://digital-strategy.ec.europa.eu/en/policies/simpl).

## Access

Just request access on the group level and our admins will validate internally